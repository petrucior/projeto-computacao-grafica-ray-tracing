/**
 * \file TesteClasseLuz.cpp
 *
 * \brief Este arquivo tem como objetivo verificar se a classe luz esta funcionando perfeitamente.
 * Compilação dos arquivos: g++ TesteClasseLuz.cpp -o t -lm vetor.hpp vetor.cpp raio.hpp raio.cpp luz.hpp luz.cpp objeto.hpp objeto.cpp
 *
 * \author 
 * Petrúcio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computação e Automação Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.0
 * \date Outubro 2013
 */
 
#include <iostream>             //cin e cout
//#include <stdlib.h>           //abs
#include "vetor.hpp"	//rayTracing::Vetor
#include "objeto.hpp"	//rayTracing::Objeto                  
#include "raio.hpp"     //rayTracing::Raio
#include "luz.hpp"	//rayTracing::Luz
using rayTracing::Vetor;
using rayTracing::Objeto;
using rayTracing::Raio;
using rayTracing::Luz;

using namespace std;

int main(){
	//Criando uma esfera
	Vetor* c_esfera = new Vetor();
	c_esfera->valores_vetor(2.0, 0.0, 0.0);
	Vetor* cores =  new Vetor();
	cores->valores_vetor(0.0, 1.0, 0.0);
	Objeto* esfera = new Objeto();
	esfera->atualizar_esfera(c_esfera, 1.0, 0.1, 0.05, cores);
	
	//Lookfrom
	Vetor* lookfrom = new Vetor();
	lookfrom->valores_vetor(2.0, 3.0, 2.0);
	//Lookat
	Vetor* lookat = new Vetor();
	lookat->valores_vetor(2.0, 1.0, 0.0);
	
	//Raio
    Raio* r = new Raio();
    r->atualizar_vetores(lookfrom, lookat, esfera);
	cout << ("Valor de t") << endl;					 
    cout << (r->calcula_t()) << endl;
	
	//Interseccao com a esfera
    Vetor* int_esfera = new Vetor();
    int_esfera = r->interseccao_esfera(r->calcula_t());
    
    //Luz
    Luz* luz = new Luz();
    luz->posicao_luz(3.0, 3.0, 3.0);
    
    luz->atualizar_vetores_auxiliares(int_esfera, c_esfera, lookfrom);
    
    luz->atualizar_constantes_phong(0.4, 0.3, 0.3, 200.0, 250.0, 0.0, 0.0 , 1.0, 1.0);
    
    double valor_luz = luz->calcula_luz_red();
    
    cout << ("O valor da luz eh: ");
    cout << (valor_luz) << endl;
    
    return 0;
}