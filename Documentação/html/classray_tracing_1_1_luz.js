var classray_tracing_1_1_luz =
[
    [ "Luz", "classray_tracing_1_1_luz.html#a5e598fa590654c51034123a7b775dd32", null ],
    [ "atualizar_constantes_phong", "classray_tracing_1_1_luz.html#a507188ef2e57f21c7caa4f481b777fb3", null ],
    [ "atualizar_vetores_auxiliares", "classray_tracing_1_1_luz.html#af99edd67480e36cea83753b818c393bc", null ],
    [ "calcula_luz_blue", "classray_tracing_1_1_luz.html#a6af6111ef9af6c9a81fb4f865414944e", null ],
    [ "calcula_luz_green", "classray_tracing_1_1_luz.html#a9b462d8f50d679a9e69c6d55f06d2c7f", null ],
    [ "calcula_luz_red", "classray_tracing_1_1_luz.html#a125f8030e5b49135d4da8df13e2bc27f", null ],
    [ "posicao_luz", "classray_tracing_1_1_luz.html#a471ecc09c09629600a696efa6f6f05d9", null ],
    [ "vetor_luz", "classray_tracing_1_1_luz.html#a2139306c30f73e1cec28322f29f16b75", null ],
    [ "vetor_normal", "classray_tracing_1_1_luz.html#a76cfb34477865fb6e3d353996aaaf67e", null ],
    [ "vetor_observador", "classray_tracing_1_1_luz.html#ad7a4688f8791b58fafb73aa794672782", null ],
    [ "vetor_reflexao", "classray_tracing_1_1_luz.html#a97c698572d891d05163e1655c8b91d79", null ]
];