var files =
[
    [ "cena.cpp", "cena_8cpp.html", null ],
    [ "cena.hpp", "cena_8hpp.html", [
      [ "Cena", "classray_tracing_1_1_cena.html", "classray_tracing_1_1_cena" ]
    ] ],
    [ "luz.cpp", "luz_8cpp.html", null ],
    [ "luz.hpp", "luz_8hpp.html", [
      [ "Luz", "classray_tracing_1_1_luz.html", "classray_tracing_1_1_luz" ]
    ] ],
    [ "main.cpp", "main_8cpp.html", "main_8cpp" ],
    [ "objeto.cpp", "objeto_8cpp.html", null ],
    [ "objeto.hpp", "objeto_8hpp.html", [
      [ "Objeto", "classray_tracing_1_1_objeto.html", "classray_tracing_1_1_objeto" ]
    ] ],
    [ "raio.cpp", "raio_8cpp.html", null ],
    [ "raio.hpp", "raio_8hpp.html", [
      [ "Raio", "classray_tracing_1_1_raio.html", "classray_tracing_1_1_raio" ]
    ] ],
    [ "ray_tracing.cpp", "ray__tracing_8cpp.html", null ],
    [ "ray_tracing.hpp", "ray__tracing_8hpp.html", [
      [ "Ray_tracing", "classray_tracing_1_1_ray__tracing.html", "classray_tracing_1_1_ray__tracing" ]
    ] ],
    [ "textura.cpp", "textura_8cpp.html", null ],
    [ "textura.hpp", "textura_8hpp.html", [
      [ "Textura", "classray_tracing_1_1_textura.html", "classray_tracing_1_1_textura" ]
    ] ],
    [ "vetor.cpp", "vetor_8cpp.html", null ],
    [ "vetor.hpp", "vetor_8hpp.html", [
      [ "Vetor", "classray_tracing_1_1_vetor.html", "classray_tracing_1_1_vetor" ]
    ] ]
];