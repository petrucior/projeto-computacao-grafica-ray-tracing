var searchData=
[
  ['altura',['altura',['../classray_tracing_1_1_cena.html#ab234e7f29df2800031d8da58e06cddb9',1,'rayTracing::Cena']]],
  ['angulo',['angulo',['../classray_tracing_1_1_vetor.html#ab6bb71aec70d79eb289cd8a727077314',1,'rayTracing::Vetor']]],
  ['atualizar_5fconstantes_5fphong',['atualizar_constantes_phong',['../classray_tracing_1_1_luz.html#a507188ef2e57f21c7caa4f481b777fb3',1,'rayTracing::Luz']]],
  ['atualizar_5fcor_5fbackground',['atualizar_cor_background',['../classray_tracing_1_1_cena.html#af04937202756dd641844e993a0fd6e72',1,'rayTracing::Cena']]],
  ['atualizar_5fesfera',['atualizar_esfera',['../classray_tracing_1_1_objeto.html#a56180145d41ebaff76984afaed75a9c3',1,'rayTracing::Objeto']]],
  ['atualizar_5fka',['atualizar_ka',['../classray_tracing_1_1_cena.html#a213ba79ed5b996fd952bef32ba722cc2',1,'rayTracing::Cena']]],
  ['atualizar_5fplano',['atualizar_plano',['../classray_tracing_1_1_objeto.html#a144ae34bc4d6a28918df9f01b34b0acd',1,'rayTracing::Objeto']]],
  ['atualizar_5fvetores',['atualizar_vetores',['../classray_tracing_1_1_raio.html#aace8c41a593b670ded636c2ed52b79fe',1,'rayTracing::Raio']]],
  ['atualizar_5fvetores_5fauxiliares',['atualizar_vetores_auxiliares',['../classray_tracing_1_1_luz.html#af99edd67480e36cea83753b818c393bc',1,'rayTracing::Luz']]]
];
