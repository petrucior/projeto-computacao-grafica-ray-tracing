var searchData=
[
  ['calcula_5fluz_5fblue',['calcula_luz_blue',['../classray_tracing_1_1_luz.html#a6af6111ef9af6c9a81fb4f865414944e',1,'rayTracing::Luz']]],
  ['calcula_5fluz_5fgreen',['calcula_luz_green',['../classray_tracing_1_1_luz.html#a9b462d8f50d679a9e69c6d55f06d2c7f',1,'rayTracing::Luz']]],
  ['calcula_5fluz_5fred',['calcula_luz_red',['../classray_tracing_1_1_luz.html#a125f8030e5b49135d4da8df13e2bc27f',1,'rayTracing::Luz']]],
  ['cena',['Cena',['../classray_tracing_1_1_cena.html#a5cc5ea74e2dab0064399282049a13add',1,'rayTracing::Cena']]],
  ['cor_5fbackground_5fb',['cor_background_b',['../classray_tracing_1_1_cena.html#a76001e5f164dbe9307dd63fcc94f3e16',1,'rayTracing::Cena']]],
  ['cor_5fbackground_5fg',['cor_background_g',['../classray_tracing_1_1_cena.html#ad7b3e67053240ab88ec287870d117bee',1,'rayTracing::Cena']]],
  ['cor_5fbackground_5fr',['cor_background_r',['../classray_tracing_1_1_cena.html#a9f34fd0fd1d31f71c601822d57141c58',1,'rayTracing::Cena']]],
  ['cor_5fesfera',['cor_esfera',['../classray_tracing_1_1_objeto.html#a1c81af6fdabf87cb2c727b44e8e7a726',1,'rayTracing::Objeto']]],
  ['cor_5fplano',['cor_plano',['../classray_tracing_1_1_objeto.html#abe78412c49bc5bb6120599939ef925cf',1,'rayTracing::Objeto']]]
];
