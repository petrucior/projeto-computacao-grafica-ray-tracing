/**
 * \file TesteClasseCena.cpp
 *
 * \brief Este arquivo tem como objetivo verificar se a classe cena esta funcionando perfeitamente.
 * Compila��o dos arquivos: g++ -o t TesteClasseCena.cpp -lopengl32 -lglu32 -lglut32 -lm imagem.hpp imagem.cpp
 *
 * \author 
 * Petr�cio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computa��o e Automa��o Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.0
 * \date Outubro 2013
 */
 
#include <iostream>             //cin e cout		
#include "cena.hpp"		//rayTracing::Cena
#include "objeto.hpp"	//rayTracing::Objeto
#include "vetor.hpp"	//rayTracing::Vetor

using rayTracing::Cena;
using rayTracing::Objeto;
using rayTracing::Vetor;

using namespace std;

int main(){
	//Primeira esfera
	Vetor* c_esfera = new Vetor();
	c_esfera->valores_vetor(2.0,2.0,1.0);
	Vetor* cores = new Vetor();
	cores->valores_vetor(1.0, 0.0, 0.0);
    Objeto* esfera = new Objeto();
	esfera->atualizar_esfera(c_esfera, 1.0, 0.0, 1.0, cores);
	cout << (esfera->ks_esfera()) << endl;
	
	//Segunda esfera
	Vetor* c_esfera2 = new Vetor();
	c_esfera2->valores_vetor(1.0, 1.0, 4.0);
	Vetor* cores2 =  new Vetor();
	cores2->valores_vetor(0.0, 1.0, 0.0);
	Objeto* esfera2 = new Objeto();
	esfera2->atualizar_esfera(c_esfera2, 2.0, 0.1, 0.05, cores2);
	cout << (esfera2->ks_esfera()) << endl;
	
	Cena* cena = new Cena();
	cena->atualizar_cor_background(0.0, 0.0, 0.0);
	cena->atualizar_ka(1.2);
	cena->incluir_objetos_pilha(esfera);
	cena->incluir_objetos_pilha(esfera2);
	Objeto* aux = cena->excluir_objetos_pilha();
	cout << (aux->ks_esfera()) << endl;
	Objeto* aux2 = cena->excluir_objetos_pilha();
	cout << (aux2->ks_esfera()) << endl;
	Objeto* aux3 = cena->excluir_objetos_pilha();
	//Se n�o tem mais objetos na lista retorna NULL
	cout << (aux3) << endl;
    return 0;
}