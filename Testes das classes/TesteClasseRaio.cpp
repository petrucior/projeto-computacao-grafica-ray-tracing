/**
 * \file TesteClasseRaio.cpp
 *
 * \brief Este arquivo tem como objetivo verificar se a classe Raio est? funcionando perfeitamente.
 * Compila��o dos arquivos: g++ TesteClasseRaio.cpp -o t -lm vetor.hpp vetor.cpp raio.hpp raio.cpp objeto.hpp objeto.cpp
 *
 * \author 
 * Petr�cio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computa��o e Automa��o Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.0
 * \date Outubro 2013
 */
 
#include <iostream>             //cin e cout
//#include <stdlib.h>           //abs
#include "vetor.hpp"	//rayTracing::Vetor                   
#include "raio.hpp"     //rayTracing::Raio
#include "objeto.hpp"	//rayTracing::Objeto

using rayTracing::Vetor;
using rayTracing::Raio;
using rayTracing::Objeto;

using namespace std;

int main(){
	//Criando uma esfera
	Vetor* c_esfera = new Vetor();
	c_esfera->valores_vetor(2.0, 0.0, 0.0);
	Vetor* cores =  new Vetor();
	cores->valores_vetor(0.0, 1.0, 0.0);
	Objeto* esfera = new Objeto();
	esfera->atualizar_esfera(c_esfera, 1.0, 0.1, 0.05, cores);
	
	//Lookfrom
	Vetor* lookfrom = new Vetor();
	lookfrom->valores_vetor(2.0, 3.0, 2.0);
	//Lookat
	Vetor* lookat = new Vetor();
	lookat->valores_vetor(2.0, 1.0, 0.0);
		
	
    Raio* r = new Raio();
    r->atualizar_vetores(lookfrom, lookat, esfera);
	cout << ("Valor de t") << endl;
    cout << (r->calcula_t()) << endl;
	
	cout << ("Interseccao") << endl;
	Vetor* v = new Vetor();
	v = r->interseccao_esfera(r->calcula_t());
	double x, y, z;
	x = v->vx();
	y = v->vy();
	z = v->vz();
	cout << (x) << endl;
	cout << (y) << endl;
	cout << (z) << endl;
    return 0;
}