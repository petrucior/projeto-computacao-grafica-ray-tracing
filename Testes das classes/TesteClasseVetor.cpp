/**
 * \file TesteClasseVetor.cpp
 *
 * \brief Este arquivo tem como objetivo verificar se a classe Vetor est� funcionando perfeitamente.
 * Compila��o dos arquivos: g++ TesteClasseVetor.cpp -o t -lm vetor.hpp vetor.cpp
 *
 * \author 
 * Petr�cio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computa��o e Automa��o Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.0
 * \date Outubro 2013
 */
 
#include <iostream>		//cin e cout
//#include <stdlib.h>		//abs			
#include "vetor.hpp"	//rayTracing::Vetor
using rayTracing::Vetor;

using namespace std;

int main(){
	Vetor* v = new Vetor();
	v->valores_vetor(4.0, 1.0, 6.0);
	Vetor* x = new Vetor();
	x->valores_vetor(-3.0, 0.0, 2.0);
	Vetor* drt = new Vetor();
	cout << (v->vz()) << endl;
	cout << (x->produto_escalar(v)) << endl;
	cout << (v->norma()) << endl;
	cout << (x->norma()) << endl;
	cout << (x->angulo(v));
	return 0;
}
