/**
 * \file TesteClasseObjeto.cpp
 *
 * \brief Este arquivo tem como objetivo verificar se a classe objeto esta funcionando perfeitamente.
 * Compila��o dos arquivos: g++ TesteClasseObjeto.cpp -o t -lm vetor.hpp vetor.cpp objeto.hpp objeto.cpp
 *
 * \author 
 * Petr�cio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computa��o e Automa��o Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.0
 * \date Outubro 2013
 */
 
#include <iostream>             //cin e cout
#include "objeto.hpp"	//rayTracing::Objeto
#include "vetor.hpp"	//rayTracing::Vetor

using rayTracing::Objeto;
using rayTracing::Vetor;

using namespace std;

int main(){
	Vetor* c_esfera = new Vetor();
	c_esfera->valores_vetor(2.0,2.0,1.0);
	Vetor* cores = new Vetor();
	cores->valores_vetor(1.0, 0.0, 0.0);
    Objeto* esfera = new Objeto();
	esfera->atualizar_esfera(c_esfera, 1.0, 0.0, 1.0, cores);
	cout << (esfera->ks_esfera());
    return 0;
}