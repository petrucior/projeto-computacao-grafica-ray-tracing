#ifndef WIN32	//Windows
#	include <w32api/GL/gl.h>
#	include <w32api/GL/glut.h>
#else	//Unix
#	include <GL/GL.h>
#	include <GL/GLU.h>
#	include <GL/glut.h>
#endif
//Compilando gcc -o t teste.c -lopengl32 -lglu32 -lglut32
//Lembrar de excluir o processo no gerenciador, porque ele fica aberto

void init(void) 
{
   /* select clearing colors */
   glClearColor (0.0, 0.0, 0.0, 0.0);
}

void display(void)
{
   /* clear all pixels */
   glClear (GL_COLOR_BUFFER_BIT);

   /* clear the modeling stack matrix */
   glLoadIdentity();

   /* set the observer */
   gluLookAt (0.0, 0.0, 3.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

   /* draw a white */
   glColor3f (1.0, 0.0, 0.0);
   
   /* wireframed sphere */
   glutWireSphere (1.0, 20, 20);

   glFlush ();
}

void reshape (int w, int h)
{
   /* set the viewpor dimensions */
   glViewport (0, 0, (GLsizei) w, (GLsizei) h); 
   /* set the viewing parameters */
   glMatrixMode (GL_PROJECTION);
   /* clear the projection stack matrix */
   glLoadIdentity ();
   glFrustum (-1.5, 1.5, -1.5, 1.5, 1.5, 20.0);
   /* restore the modeling matrix mode */
   glMatrixMode (GL_MODELVIEW);
}

/* ARGSUSED1 */
void keyboard(unsigned char key, int x, int y)
{
   switch (key) {
      case 27:
         exit(0);
         break;
   }
}

int main(int argc, char** argv)
{
   glutInit(&argc, argv);
   glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
   glutInitWindowSize (350, 350); 
   glutInitWindowPosition (100, 100);
   glutCreateWindow (argv[0]);
   init ();
   glutDisplayFunc(display); 
   glutReshapeFunc(reshape);
   glutKeyboardFunc(keyboard);
   glutMainLoop();
   return 0;
}