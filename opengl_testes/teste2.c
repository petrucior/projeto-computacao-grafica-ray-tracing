#include <stdio.h>
#include <w32api/GL/gl.h>
#include <w32api/GL/glut.h>
#include "GLAUX.h"
//Compilando gcc -o t teste2.c -lopengl32 -lglu32 -lglut32 GLAUX.h
//Lembrar de excluir o processo no gerenciador, porque ele fica aberto

void init(void) 
{
   /* select clearing colors */
   glClearColor (0.0, 0.0, 0.0, 0.0);
}

void display(void)
{
   /* clear all pixels */
   glClear (GL_COLOR_BUFFER_BIT);

   /* clear the modeling stack matrix */
   glLoadIdentity();
   
   glColor3f(1.0, 1.0, 0.0);
   
   glBegin(GL_POLYGON);
   glVertex3f (0.25, 0.25, 0.0);
   glVertex3f (0.75, 0.25, 0.0);
   glVertex3f (0.75, 0.75, 0.0);
   glVertex3f (0.25, 0.75, 0.0);
   glEnd();

   glFlush ();
}

void reshape (int w, int h)
{
   /* set the viewpor dimensions */
   glViewport (0, 0, (GLsizei) w, (GLsizei) h);
}

int main(int argc, char** argv)
{
   glutInit(&argc, argv);
   glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
   glutInitWindowSize (350, 350); 
   glutInitWindowPosition (100, 100);
   init ();
   glutCreateWindow (argv[0]);
   glutDisplayFunc(display); 
   glutReshapeFunc(reshape);
   glutMainLoop();
   return 0;
}